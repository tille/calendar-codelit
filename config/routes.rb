Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :remainders
      resources :months, only: [:index]

      get 'getRemainders/:id', to: 'days#getRemainders'
      get 'getDays/:id', to: 'months#getDays'
    end
  end
end
