# README

Codelitt calendar

backend: it is working using mysqlite you can run it by doing
`bundle install && rake db:setup && rails s`

front: it is created as a subfolder called "front", you can run it by doing:
`cd front && npm i && npm start`

Full features implemented... there are improvements but in terms of code refactoring and performance

rspec was not required since my ruby models don't have code to be tested just the associations
however I can test the front-end components with jest, this is something i'm implementing in the time being

I'll place some pictures in screenshots folder, showing all the required features working

BONUS:
If provide thoughts on what you could improve on your code given more time and incentives

- Jest
- upload it to a server
- there are some good candidates to be refactored for instance the api calls can be reduced by bringing the associations of a model instead of making two calls to the server