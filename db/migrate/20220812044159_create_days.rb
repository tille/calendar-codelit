class CreateDays < ActiveRecord::Migration[7.0]
  def change
    create_table :days do |t|
      t.integer :day_number
      t.belongs_to :month, index: true

      t.timestamps
    end
  end
end
