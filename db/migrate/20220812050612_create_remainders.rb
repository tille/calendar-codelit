class CreateRemainders < ActiveRecord::Migration[7.0]
  def change
    create_table :remainders do |t|
      t.string :time
      t.string :color
      t.string :description
      t.belongs_to :day, index: true

      t.timestamps
    end
  end
end
