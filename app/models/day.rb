class Day < ApplicationRecord
    has_many :remainders, dependent: :destroy
    belongs_to :month
end
