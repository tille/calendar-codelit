class Month < ApplicationRecord
    has_many :days, dependent: :destroy
end
