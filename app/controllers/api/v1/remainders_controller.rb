class Api::V1::RemaindersController < ApplicationController
  before_action :set_remainder, only: %i[ show update destroy ]

  def index
    @remainders = Remainder.all

    render json: @remainders
  end

  def show
    render json: @remainder
  end

  def create
    @remainder = Remainder.new(remainder_params)

    if @remainder.save
      render json: @remainder, status: :created
    else
      render json: @remainder.errors, status: :unprocessable_entity
    end
  end

  def update
    if @remainder.update(remainder_params)
      render json: @remainder
    else
      render json: @remainder.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @remainder.destroy
  end

  private
    def set_remainder
      @remainder = Remainder.find(params[:id])
    end

    def remainder_params
      params.require(:remainder).permit(:id, :color, :time, :description, :day_id)
    end
end
