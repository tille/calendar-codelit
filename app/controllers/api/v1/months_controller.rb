class Api::V1::MonthsController < ApplicationController

    # GET /months
    def index
      @months = Month.all.select(:id, :name)

      render json: @months
    end

    def getDays
      @days = Month.find(params[:id]).days
      render json: @days
    end
end