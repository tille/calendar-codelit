class Api::V1::DaysController < ApplicationController

    def getRemainders
        @remainders = Day.find(params[:id]).remainders
        render json: @remainders
    end
end