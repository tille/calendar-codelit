import React from 'react'
import './Dashboard.css';
import { useState, useEffect } from 'react';
import axios from 'axios';
import Month from '../Month/Month';

function getAPIdata() {
    const API_URL = "http://localhost:3000/api/v1/months"
    return axios.get(API_URL).then((response) => response.data)
}

function getAPIdays(monthId) {
    const API_URL = `http://localhost:3000/api/v1/getDays/${monthId}`
    return axios.get(API_URL).then((response) => response.data)
}

export default function Dashboard() {
    const [months, setMonths] = useState([])
    const [showMonth, setShowMonth] = useState(false)
    const [monthDays, setMonthDays] = useState([])
    const [currentMonth, setCurrentMonth] = useState(-1)

  useEffect(() => {
    let mounted = true;
    getAPIdata().then((items) => {
      if (mounted) {
        setMonths(items);
      }
    });
    return () => (mounted = false);
  }, []);

  const onMonthClick = (month) => {
      getAPIdays(month.id).then((monthDays) => {
        setShowMonth(true);
        setMonthDays(monthDays)
        setCurrentMonth(month.id)
    })
  }

  const monthNames = months.map((month) => (
    <div className="monthTile" onClick={() => onMonthClick(month)}>
        {month.name}
    </div>
   ));

   const onTitleClick = (e) => {
    e.preventDefault();
    if (e.target.id == "codelitt-title") {
        setShowMonth(false);
        setMonthDays([]);
    }
   }

   const changeMonth = (delta) => {
    getAPIdays(currentMonth + delta).then((monthDays) => {
        setCurrentMonth((state) => state + delta)
        setMonthDays(monthDays)
    })
   }

   const getMonthName = () => {
    let month = months[currentMonth - 1];
    if (month) {
        return month.name;
    }
   }

  return (
    <>
        <h1 className="calendar-title" id="codelitt-title" onClick={onTitleClick}>
            { showMonth && currentMonth > 1 &&
                <span className="change-month" onClick={() => changeMonth(-1)}>
                    &#8249;&#8249;
                </span>
            }

            Calendar Codelitt

            { showMonth && currentMonth < 12 &&
                <span className="change-month" onClick={() => changeMonth(1)}>
                        &#8250;&#8250;
                </span>
            }
        </h1>

        { !showMonth &&
            <>
                <div>
                    <p className="calendar-description">
                        Tehcnical assesment calendar - It is a basic interface but I guess it is ok for the scope of the test
                    </p>
                </div>
                <div className="months-container">{ monthNames }</div>
            </>
        }

        { showMonth &&
            <Month days={monthDays} monthName={getMonthName()} />
        }
    </>
  )
}
