import React from 'react'
import Day from '../Day/Day'
import './Month.css'

const names =  [
  "Monday", "Tuesday", "Wednesday", "Thrusday", "Friday", "Saturday", "Sunday"
];

export default function Month({ days, monthName }) {

  const getContents = (from, to) => days.slice(from, to).reduce((memo, day) => {
      memo.push(<div className="column">
        <Day key={day.id.toString()} day={day} />
      </div>)
      return memo;
  }, [])

  const getNames = () => names.reduce((memo, name) => {
    memo.push(<div className="column">
      <span className="calendar-title"><b>{name}</b></span>
    </div>)
    return memo;
  }, [])

  const getWeek = (from, to) => <div className="row">{getContents(from, to)}</div>

  const getCalendar = () => {
    var rows = [];
    var prev = 0;
    for (var i = 7; i < days.length; i += 7) {
      rows.push(getWeek(prev, i));
      prev = i;
    }
    if (prev < days.length) {
      rows.push(getWeek(prev, days.length));
    }
    return rows;
  }

  return (
      <>
        <h2>{ monthName }</h2>
        <div className="calendar-container">
          <div className="row">
            {getNames()}
          </div>
        </div>

        <div className="calendar-container">
          {getCalendar()}
        </div>
      </>
  )
}
