import React from 'react'
import './Day.css'
import Breadcrumb from '../Remainder/Breadcrumb'
import Modal from 'react-modal';

import axios from 'axios';
import { useEffect, useState, useRef } from 'react';

function getAPIdata(dayID) {
  const API_URL = `http://localhost:3000/api/v1/getRemainders/${dayID}`;
  return axios.get(API_URL).then((response) => response.data)
}

function createRemainder(remainder) {
  const API_POST_URL = `http://localhost:3000/api/v1/remainders`;
  return axios.post(API_POST_URL, remainder).then((response) => response.data)
}

export default function Day({ day }) {
  const [remainders, setRemainders] = useState([]);
  const descriptionRef = useRef(null);
  const timeRef = useRef(null);
  const [modalIsOpen, setIsOpen] = useState(false);
  const [showRemaindersList, setShowRemainders] = useState(false)

  useEffect(() => {
    let mounted = true;
    getAPIdata(day.id).then((items) => {
      if (mounted) {
        setRemainders(items);
      }
    });
    return () => (mounted = false);
  }, [day]);

  const updateDeleteRemainder = (remainder_id, remainder) => {
    let newArray = [...remainders]
    const index = newArray.findIndex((element) => element.id === remainder_id);

    if (remainder === null) {
      newArray.splice(index, 1);
    } else {
      newArray.splice(index, 1, remainder);
    }
    setRemainders(newArray)
  }

  const getContents = remainders.slice(0, 3).reduce((memo, remainder) => {
    memo.push(<Breadcrumb remainder={remainder} updateRemainders={updateDeleteRemainder} />)
    return memo;
  }, [])

  const handleSubmit = (e) => {
    e.preventDefault();
    let color = document.querySelector("#remainder-color").value;
    let time = timeRef.current.value;
    let description = descriptionRef.current.value;

    createRemainder({
      color: color,
      time: time,
      description: description,
      day_id: day.id
    }).then((remainder) => {
      setRemainders((remainders) => [...remainders, remainder])
    })
    setIsOpen(false);
  }

  const handleClose = (e) => {
    e.preventDefault();
    setIsOpen(false);
    setShowRemainders(false);
  }

  const handleOpen = (e) => {
    e.preventDefault();
    if (e.target.id === "calendar-day") {
      setIsOpen(true);
    }

    if (e.target.id === "moreButton") {
      setShowRemainders(true);
    }
  }

  const handleOpenList = (e) => {
    e.preventDefault();
    if (e.target.id === "day-number") {
      setShowRemainders(true);
    }
  }

  const moreButton = () => {
    if (remainders.length > 3) {
      const num = remainders.length - 3;
      return <div className="moreButton" id="moreButton" onClick={handleOpen}>{`${num}`} more item</div>
    }
  }

  const getAllContents = remainders.reduce((memo, remainder) => {
    memo.push(<Breadcrumb remainder={remainder} />)
    return memo;
  }, [])

  return (
      <>
        <div key={day.id} className="calendar-day" id="calendar-day" onClick={handleOpen}>
          <div key={day.id} className="day-number" id="day-number" onClick={handleOpenList}>{day.day_number}</div>
          {getContents}
          {moreButton()}
        </div>

        <Modal
          isOpen={showRemaindersList}
          id="remainders-list"
        >
          <h2>List of Events</h2>
          {
            getAllContents
          }

          <div className="remainders-list-button-container">
            <button onClick={handleClose}>Close</button>
          </div>
        </Modal>

        <Modal
          isOpen={modalIsOpen}
          id="modal"
        >
          <h2>Create a Remainder</h2>
          <input
            ref={descriptionRef}
            type="text"
            id="remainder-description"
            placeholder="Ingresa una descripcion"
            maxLength="23"
          />

          <input
            ref={timeRef}
            type="text"
            id="remainder-time"
            placeholder="Ingresa una hora en formato 1:00pm"
            maxLength="8"
          />

          <label>Pick a color: </label>
          <select id="remainder-color">
            <option value="lavender">Lavender</option>
            <option value="lightBlue">Lightblue</option>
            <option value="mistyRose">Mistyrose</option>
          </select>

          <div>
            <button onClick={handleSubmit}>Create Remainder</button>
            <button onClick={handleClose}>Close</button>
          </div>
        </Modal>
      </>
  )
}
