import React from 'react'
import './Breadcrumb.css'
import Modal from 'react-modal';
import { useState, useRef } from 'react';
import axios from 'axios';

export default function Breadcrumb({ remainder, updateRemainders }) {
  let [modalIsOpen, setIsOpen] = useState(false);
  const descriptionRef = useRef(null);
  const timeRef = useRef(null);

  const showModal = () => setIsOpen(true);
  const closeModal = () => setIsOpen(false);
  const getSelected = (color) => (color === remainder.color) ? 'selected' : '';

  const createRemainder = (remainderData) => {
    const API_PUT_URL = `http://localhost:3000/api/v1/remainders/${remainder.id}`;
    return axios.put(API_PUT_URL, remainderData).then((response) => response.data)
  }

  const deleteRemainder = () => {
    const API_PUT_URL = `http://localhost:3000/api/v1/remainders/${remainder.id}`;
    return axios.delete(API_PUT_URL);
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    let color = document.querySelector("#remainder-color").value;
    let time = timeRef.current.value;
    let description = descriptionRef.current.value;

    createRemainder({
      color: color,
      time: time,
      description: description,
      day_id: remainder.day_id
    }).then((remainderResponse) => {
      remainder = remainderResponse;
      updateRemainders(remainder.id, remainder);
    })
    setIsOpen(false);
  }

  const handleDelete = (e) => {
    e.preventDefault();
    deleteRemainder();
    updateRemainders(remainder.id, null);
  }

  return (
    <>
      <div onClick={showModal} className={`description ${remainder.color}`}>
        {remainder.time} - {remainder.description}
      </div>

      <Modal isOpen={modalIsOpen} id="modal">
        <h2>Update a Remainder</h2>
        <input ref={descriptionRef}
          defaultValue={remainder.description}
          maxLength="23"
        />

        <input ref={timeRef}
          defaultValue={remainder.time}
          maxLength="8"
        />

        <label>Pick a color: </label>
        <select id="remainder-color">
          <option value="lavender" selected={getSelected('lavender')}>Lavender</option>
          <option value="lightBlue" selected={getSelected('lightBlue')}>Lightblue</option>
          <option value="mistyRose" selected={getSelected('mistyRose')}>Mistyrose</option>
        </select>

        <div>
          <button onClick={handleSubmit}>Update Remainder</button>
          <button onClick={handleDelete}>Delete</button>
          <button onClick={closeModal}>Close</button>
        </div>
      </Modal>
    </>
  )
}
