import Dashboard from './Dashboard/Dashboard.js';

function App() {
  return (
    <div className="App">
      <Dashboard />
    </div>
  );
}

export default App;
